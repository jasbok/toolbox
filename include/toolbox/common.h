//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include "toolbox/common/circular_list.h"
#include "toolbox/common/colour.h"
#include "toolbox/common/coords.h"
#include "toolbox/common/event.h"
#include "toolbox/common/exception.h"
#include "toolbox/common/file.h"
#include "toolbox/common/image.h"
#include "toolbox/common/log.h"
#include "toolbox/common/str.h"
#include "toolbox/common/vec.h"
