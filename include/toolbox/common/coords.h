//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

namespace tb {
template<typename T, typename P = struct vec2_phantom>
struct vec2 { T x, y; };

template<typename T, typename P1, typename P2>
bool
operator==(const vec2<T, P1>& a, const vec2<T, P2>& b) {
	return a.x == b.x && a.y == b.y;
}

template<typename T, typename P1, typename P2>
bool
operator!=(const vec2<T, P1>& a, const vec2<T, P2>& b) {
	return !(a == b);
}

template<typename T, typename P1, typename P2>
vec2<T, P1>
operator*(const vec2<T, P1>& a, const vec2<T, P2>& op) {
	return {static_cast<T>(a.x * op.x), static_cast<T>(a.y * op.y)};
}

template<typename T1, typename P1, typename T2, typename P2>
vec2<T1, P1>
operator*(const vec2<T1, P1>& a, const vec2<T2, P2>& op) {
	return {static_cast<T1>(a.x * op.x), static_cast<T1>(a.y * op.y)};
}

template<typename T, typename P, typename O>
vec2<T, P>
operator*(const vec2<T, P>& a, const O& op) {
	return {static_cast<T>(a.x * op), static_cast<T>(a.y * op)};
}

template<typename T, typename P1, typename P2>
vec2<T, P1>
operator+(const vec2<T, P1>& a, const vec2<T, P2>& op) {
	return {static_cast<T>(a.x + op.x), static_cast<T>(a.y + op.y)};
}

template<typename T1, typename P1, typename T2, typename P2>
vec2<T1, P1>
operator+(const vec2<T1, P1>& a, const vec2<T2, P2>& op) {
	return {static_cast<T1>(a.x + op.x), static_cast<T1>(a.y + op.y)};
}

template<typename T1, typename P1, typename T2, typename P2>
vec2<T1, P1>
operator-(const vec2<T1, P1>& a, const vec2<T2, P2>& op) {
	return {static_cast<T1>(a.x - op.x), static_cast<T1>(a.y - op.y)};
}

template<typename T, typename P, typename O>
vec2<T, P>
operator+(const vec2<T, P>& a, const O& op) {
	return {static_cast<T>(a.x + op), static_cast<T>(a.y + op)};
}

template<typename T, typename P, typename O>
vec2<T, P>
operator-(const vec2<T, P>& a, const O& op) {
	return {static_cast<T>(a.x - op), static_cast<T>(a.y - op)};
}

template<typename T, typename P = struct vec3_phantom>
struct vec3 { T x, y, z; };

template<typename T, typename P1, typename P2>
bool
operator==(const vec3<T, P1>& a, const vec3<T, P2>& b) {
	return a.x == b.x && a.y == b.y && a.z == b.z;
}

template<typename T, typename P1, typename P2>
bool
operator!=(const vec3<T, P1>& a, const vec3<T, P2>& b) {
	return !(a==b);
}

template<typename T, typename P, typename O>
vec3<T, P>
operator*(const vec3<T, P>& a, const O& op) {
	return {
	        static_cast<T>(a.x * op),
	        static_cast<T>(a.y * op),
	        static_cast<T>(a.z * op)
	};
}

template<typename T, typename P, typename O>
vec3<T, P>
operator+(const vec3<T, P>& a, const O& op) {
	return {
	        static_cast<T>(a.x + op),
	        static_cast<T>(a.y + op),
	        static_cast<T>(a.z + op)
	};
}

template<typename T>
using dims2 = vec2<T, struct dims2_phantom>;

template<typename T>
T
area(const dims2<T>& a) {
	return a.x * a.y;
}

template<typename T>
using dims3 = vec3<T, struct dims3_phantom>;

template<typename T>
T
area(const dims3<T>& a) {
	return a.x * a.y * a.z;
}

template<typename T>
using coords2 = vec2<T, struct coords2_phantom>;

template<typename T>
using coords3 = vec3<T, struct coords3_phantom>;

template<typename C, typename D>
struct rect { coords2<C> coords; dims2<D> dims; };

template<typename T>
struct rect<T, T>
from(const vec2<T>& top_left, const vec2<T>& bottom_right) {
	return { top_left, bottom_right - top_left };
}

template<typename C, typename D>
struct box { coords3<C> coords; dims3<D> dims; };

template<typename T>
struct box<T, T>
from(const vec3<T>& top_left_front, const vec3<T>& bottom_right_back) {
	return { top_left_front, bottom_right_back - top_left_front };
}
} // namespace tb
