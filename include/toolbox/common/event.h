//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include "toolbox/common/range.h"

#include <functional>
#include <vector>

namespace tb {

template<typename KEY, typename VAL>
struct mapping {
	KEY key;
	VAL val;
};

inline auto
match_key(const auto& in) {
	return [&in](const auto& comp){ return in.key == comp.key; };
}

inline auto
match_val(const auto& in) {
	return [&in](const auto& comp){ return in.val == comp.val; };
}

template<typename KEY, typename ...ARGS>
class event_handler {
	// A event-handle pair.
	using entry = mapping<KEY, std::function<void (ARGS...)> >;

	// A list of event handle pairs.
	using map = std::vector<entry>;
	map handles_;

	// An ordered lists of event keys used to sort handles based on list order.
	std::vector<KEY> order_;

	// Sort handles by the order of the priority list.
	void
	sort_handles_() {
		if(!order_.empty()) {
			sort(handles_, [this](auto a, auto b){
				return find(order_, a.key) < find(order_,b.key);
			});
		}
	}

public:
	std::vector<entry>
	handles() const { return handles_; }

	void
	insert(const entry& in) {
		if(auto f = find_if(handles_, match_key(in)); f != end(handles_)) {
			f->val = in.val;
			return;
		}

		handles_.push_back(in);
		sort_handles_();
	}

	void
	insert(const map& ins) {
		for(const auto& in : ins) {
			if(auto f = find_if(handles_, match_key(in)); f != end(handles_)) {
				f->val = in.val;
				continue;
			}
			handles_.push_back(in);
		}
		sort_handles_();
	}

	void
	remove(const KEY& key) {
		std::erase_if(handles_, [&key](const entry& e){ return e.key == key;});
	}

	std::vector<KEY>
	ordering() const {
		return order_;
	}

	void
	ordering(const std::vector<KEY>& order) {
		order_ = tb::deduplicate(order);
		sort_handles_();
	}

	bool
	handle(const KEY& key, ARGS... args) {
		for(const auto& [k, v]: handles_) {
			if(k == key) {
				v(args ...);
				return true;
			}
		}
		return false;
	}
}; // class event_handler

} // namespace tb
