//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <functional>

namespace tb {

template<typename T>
class filterer {
	using filter_func = std::function<bool (T)>;
	using filter_arr = std::vector<filter_func>;

	filter_arr filters_;
public:

	filterer(const auto& filters) : filters_(filters){}

	bool
	filter(const T& in){
		for(const auto& filter : filters_) {
			if(filter(in)) {
				return true;
			}
		}
		return false;
	}
};

}
