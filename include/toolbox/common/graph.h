//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <functional>
#include <vector>

namespace tb {

template<typename T>
struct node {
	T data;
	std::vector<size_t> edges;
};

template<typename T>
using graph = std::vector<tb::node<T>>;

template<typename T>
using node_edges_map = std::vector<std::pair<T, std::vector<T>>>;

template<typename T>
graph<T>
directional_graph_from(const node_edges_map<T>& map){
	std::vector<std::pair<T, size_t>> lookup;
	lookup.reserve(map.size());
	for(size_t i = 0; const auto& p : map) {
		lookup.push_back({p.first, i++});
	}

	graph<T> gr;
	gr.reserve(map.size());

	for(const auto& p : map) {
		std::vector<size_t> edges;
		edges.reserve(p.second.size());

		for(const T& edge : p.second) {
			for(const auto& lu : lookup) {
				if(lu.first == edge) {
					edges.push_back(lu.second);
					break;
				}
			}
		}

		gr.push_back({p.first, edges});
	}

	return gr;
}


// No recursion travel, starting a parent node then edge nodes.
template<typename T>
void
travel(const graph<T>& gr,
       size_t start,
       const std::function<void(const node<T>&)>& func){
	std::vector<size_t> stack{start};
	while(!stack.empty()) {
		size_t curr = stack.back();
		func(gr[curr]);

		stack.pop_back();
		for(size_t i : gr[curr].edges) { stack.push_back(i); }
	}
}


// No recursion travel, starting at edge nodes and working up to
// parent. A lot less efficient than parent first travel.
template<typename T>
void
travel_edge_first(const graph<T>& gr,
                  size_t start,
                  const std::function<void(const node<T>&)>& func){
	std::vector<size_t> stack{start, start};
	for(size_t i : gr[start].edges) { stack.push_back(i); }

	while(!stack.empty()) {
		size_t curr = stack.back();

		// Check if we have already travelled the edge nodes.
		if(stack[stack.size()-2] == curr) {
			func(gr[curr]); stack.pop_back(); stack.pop_back();
		}
		else{
			// Place duplicate on stack to indicate that we have already
			// pushed the edge nodes.
			stack.push_back(curr);
			for(size_t i : gr[curr].edges) { stack.push_back(i); }
		}
	}
}


// Finds all loops in the graph.
template<typename T>
void
find_loops(const graph<T>& gr,
           size_t start,
           const std::function<void(std::vector<size_t>&&,
                                    std::vector<size_t>&&)>& func){
	std::vector<size_t> stack{start, start};
	for(size_t i : gr[start].edges) { stack.push_back(i); }

	std::vector<size_t> loop{start};
	while(!stack.empty()) {
		size_t curr = stack.back();

		// Check if we have already travelled the edge nodes.
		if(stack[stack.size()-2] == curr) {
			stack.pop_back(); stack.pop_back();
			loop.pop_back();
		}
		else{
			loop.push_back(curr);

			// Place duplicate on stack to indicate that we have already
			// pushed the edge nodes.
			stack.push_back(curr);
			for(size_t i : gr[curr].edges) {
				if(auto it = std::find(begin(loop), end(loop), i); it != end(loop)) {
					func({begin(loop), it}, {it, end(loop)});
					continue;
				}
				stack.push_back(i);
			}
		}
	}
}

} // namespace tb
