//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <functional>
#include <optional>
#include <string>
#include <vector>

namespace tb {

struct ini {
	struct kv {
		std::string key;
		std::string value;
	};

	struct section {
		std::string name;
		std::vector<kv> key_values;

		std::string&
		operator[](const std::string_view& key);

		std::vector<kv>::iterator
		begin();

		std::vector<kv>::iterator
		end();
	};

	section&
	operator[](const std::string_view& section);

	std::vector<section>::iterator
	begin();

	std::vector<section>::iterator
	end();

	std::vector<section> sections;
};

ini
read_ini(const std::string& path);

void
read_ini(const std::string& path,
         std::function<void(const std::string_view& section,
                            const std::string_view& key,
                            const std::string_view& value)>);

}
