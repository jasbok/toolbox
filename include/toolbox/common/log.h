//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <fmt/chrono.h>
#include <fmt/color.h>
#include <fmt/format.h>

#include <stdexcept>

namespace tb::logging {
enum class level {
	NONE = 0,
	EXCEPTION,
	ERROR,
	WARNING,
	INFO,
	DEBUG,
	TRACE
};

// static enum level level = tb::logging::level::DEBUG;

template<class ... ARGS>
inline void
log(const std::string& file,
    int line,
    const std::string& level,
    const std::string& mesg,
    ARGS... args) {
	auto time = fmt::localtime(std::time(nullptr));
	auto fmt_mesg = fmt::format(mesg, args ...);
	fmt::print("{:%FT%T} [{}]({}:{}) {}\n",
	           time,
	           level,
	           file,
	           line,
	           fmt_mesg);
}

inline void
log(const std::string& file,
    int line,
    const std::string& level,
    const std::string& mesg) {
	auto time = fmt::localtime(std::time(nullptr));
	fmt::print("{:%FT%T} [{}]({}:{}) {}\n", time, level, file, line, mesg);
}

template<class ... ARGS>
inline void
log(const fmt::text_style& ts,
    const std::string& file,
    int line,
    const std::string& level,
    const std::string& mesg,
    ARGS... args) {
	auto time = fmt::localtime(std::time(nullptr));
	auto fmt_mesg = fmt::format(mesg, args ...);
	fmt::print(ts,
	           "{:%FT%T} [{}]({}:{}) {}\n",
	           time,
	           level,
	           file,
	           line,
	           fmt_mesg);
}

inline void
log(const fmt::text_style& ts,
    const std::string& file,
    int line,
    const std::string& level,
    const std::string& mesg) {
	auto time = fmt::localtime(std::time(nullptr));
	fmt::print(ts,
	           "{:%FT%T} [{}]({}:{}) {}\n",
	           time,
	           level,
	           file,
	           line,
	           mesg);
}
} // namespace tb::logging

namespace tb {
struct exception : std::exception {
	std::string mesg;

	template<typename ... ARGS>
	exception(const std::string& fmt, ARGS... args) :
		mesg(fmt::format(fmt, args ...))
	{}

	const char *
	what() const throw() override {
		return mesg.c_str();
	}
};
}

#ifdef NDEBUG
#define TRACE
#define DEBUG
#define INFO
#define WARN
#define ERROR
#define EXCEPT

#else

#define __FILENAME__ (strrchr(__FILE__, '/') ? \
	              strrchr(__FILE__, '/') + 1 : __FILE__)

#define TRACE(mesg, args ...)                            \
	if(tb::logging::level >= tb::logging::level::TRACE)  \
	tb::logging::log(fmt::fg(fmt::color::gray),          \
	                 __FILENAME__,                       \
	                 __LINE__,                           \
	                 "TRACE",                            \
	                 mesg,                               \
	                 ## args)

#define DEBUG(mesg, args ...)                            \
	if(tb::logging::level >= tb::logging::level::DEBUG)  \
	tb::logging::log(fmt::fg(fmt::color::gray),          \
	                 __FILENAME__,                       \
	                 __LINE__,                           \
	                 "DEBUG",                            \
	                 mesg,                               \
	                 ## args)

#define INFO(mesg, args ...)                            \
	if(tb::logging::level >= tb::logging::level::INFO)  \
	tb::logging::log(__FILENAME__,                      \
	                 __LINE__,                          \
	                 "INFO",                            \
	                 mesg,                              \
	                 ## args)

#define WARN(mesg, args ...)                               \
	if(tb::logging::level >= tb::logging::level::WARNING)  \
	tb::logging::log(fmt::fg(fmt::color::orange),          \
	                 __FILENAME__,                         \
	                 __LINE__,                             \
	                 "WARN",                               \
	                 mesg,                                 \
	                 ## args)

#define ERROR(mesg, args ...)                             \
	if(tb::logging::level >= tb::logging::level::ERROR)   \
	tb::logging::log(fmt::fg(fmt::color::red),            \
	                 __FILENAME__,                        \
	                 __LINE__,                            \
	                 "ERROR",                             \
	                 mesg,                                \
	                 ## args)

#define EXCEPT(mesg, args ...)                               \
	if(tb::logging::level >= tb::logging::level::EXCEPTION)  \
	tb::logging::log(fmt::fg(fmt::color::red),               \
	                 __FILENAME__,                           \
	                 __LINE__,                               \
	                 "EXCEPTION",                            \
	                 mesg,                                   \
	                 ## args);                               \
	throw tb::exception (mesg, ## args)

#endif // ifdef NDEBUG
