//
// Copyright (C) 2021 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <regex>
#include <string>

namespace tb
{
std::string
replace_one(const std::string& str,
            const std::string& instance,
            const std::string& replacement);

std::string
replace_all(const std::string& str,
            const std::string& instance,
            const std::string& replacement);

std::string
replace_all(const std::string& str,
            const std::string& instance,
            const std::string& replacement);

std::string
replace_all(const std::string& str,
            const std::regex&  regex,
            const std::string& replacement);

bool
match(const std::string& str,
      const std::regex&  regex);

std::string
find_one(const std::string& str,
         const std::regex&  regex);

std::vector<std::string>
find_all(const std::string& str,
         const std::regex&  regex);

std::vector<std::vector<std::string>>
find_all_groups(const std::string& str,
                const std::regex&  regex);

std::pair<std::string_view, std::string_view>
read_kv(const std::string_view& str, char separator);

std::pair<std::string_view, std::string_view>
read_kv_trim(const std::string_view& str, char separator);

std::vector<std::string>
split(const std::string_view& str,
      const std::string_view& separator);

std::vector<std::string>
split(const std::string& str,
      const std::string& separator,
      const std::vector<std::pair<std::string, std::string>>& braces);

std::string
join(const std::vector<std::string>& strings,
     const std::string&              separator);

bool
contains(const std::string_view& str,
         const std::string&      instance);

std::string_view
trim(const std::string_view& str);

std::vector<std::string>
trim(const std::vector<std::string>& strings);

std::vector<std::string>
split_and_trim(const std::string_view& str,
               const std::string_view& separator);

bool
starts_with(const std::string_view& str, char ch);

bool
starts_with(const std::string_view& str, const std::string_view& seq);

bool
ends_with(const std::string_view& str, char ch);

bool
ends_with(const std::string_view& str, const std::string_view& seq);

bool
equals(const std::string_view& a,
       const std::string_view& b);
} // namespace tb
