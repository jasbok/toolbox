//
// Copyright (C) 2021 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <vector>

namespace tb
{
/**
 * @brief Concatenates two vectors and returns the result.
 *
 * @tparam T
 * @param a The original vector.
 * @param b The vector to append to the original vector.
 * @return std::vector<T> The result of append \a b to \a a.
 */
template<typename T>
std::vector<T>
concat ( const std::vector<T>& a, const std::vector<T>& b ) noexcept {
	std::vector<T> ret = a;

	ret.insert ( ret.end(), b.begin(), b.end() );

	return ret;
}

/**
 * @brief Append vector \a b to vector \a a in place.
 *
 * @tparam T
 * @param a The target vector.
 * @param b The vector to append to \a a.
 */
template<typename T>
void
append ( std::vector<T>& a, const std::vector<T>& b ) {
	a.insert ( a.end(), b.begin(), b.end() );
}

/**
 * @brief Attempts to cast the items of type \a T of the given vector to type
 * \a C, returning a new vector with the casted items.
 *
 * @tparam C The type to which to cast the original items.
 * @tparam T The type of items stored by the original vector \a a.
 * @param a The vector to transform.
 * @return std::vector<C> A new vector of type \a C.
 */
template<typename C, typename T>
std::vector<C>
cast ( const std::vector<T>& a ) {
	std::vector<C> b;

	b.reserve ( a.size() );
	b.insert ( b.end(), a.begin(), a.end() );

	return b;
}
} // namespace tb
