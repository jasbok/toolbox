//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include "toolbox/common/ini.h"

#include <string>
#include <vector>

namespace tb {

struct slangp {
	struct shader {
		std::string shader;
		std::string alias;

		bool filter_linear = false;
		bool mipmap_input = false;
		bool float_framebuffer = false;

		float scale = 1.0;
		float scale_x = 1.0;
		float scale_y = 1.0;

		enum class scale_type {
			source,
			viewport,
			absolute
		};
		enum scale_type scale_type = scale_type::viewport;
		enum scale_type scale_type_x = scale_type::viewport;
		enum scale_type scale_type_y = scale_type::viewport;
	};

	struct texture {
		std::string alias;
		std::string path;
		bool linear = false;
	};

	std::vector<shader> shaders;
	std::vector<texture> textures;
};

struct translation_unit {
public:
	struct preprocessor {
		enum class type { pragma, include } type;
		std::string source;
		int line;
	};

	struct file {
		std::string name;
		std::string source;
		std::vector<preprocessor> preprocessors;
		std::vector<file> includes;
	};
};

struct shader {
	struct push_constant {
		enum class type {
			vec4,
			uint
		} type;
		std::string name;
	};
	std::vector<push_constant> push_constants;

	struct ubo {
		enum class type {
			mat4
		} type;
		std::string name;
	};
	std::vector<ubo> ubos;
};

slangp
read_slangp(const std::string path);

} // namespace tb
