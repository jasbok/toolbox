//
// Copyright (C) 2021 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <nlohmann/json.hpp>

namespace tb {
/**
 * @brief Forces static assert compilation failure if to_json is not implemented
 * for type T.
 *
 * @tparam T Type to convert to JSON.
 * @param j[out] Destination object to store JSON.
 * @param t[in] Object of type T to convert to JSON.
 */
template<typename T>
void
to_json ( nlohmann::json& j, const T& t ) {
	static_assert(sizeof(T) == 0, "to_json not implemented for T.");
}

/**
 * @brief Forces static assert compilation failure if from_json is not
 * implemented for type T.
 *
 * @tparam T Type to convert to JSON.
 * @param j[in] JSON object to convert to type T.
 * @param t[out] Destination object to store converted JSON.
 */
template<typename T>
void
from_json ( const nlohmann::json& j, T& t ) {
	static_assert(sizeof(T) == 0, "from_json not implemented for T.");
}
}
