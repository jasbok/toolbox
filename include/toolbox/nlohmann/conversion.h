//
// Copyright (C) 2021 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include "toolbox/nlohmann/evaluate.h"
#include "toolbox/nlohmann/except.h"

#include <nlohmann/json.hpp>

#include <optional>

namespace tb
{
/**
 * @brief Attempt to find and convert \a field in JSON object \a j and return
 * the converted T value, otherwise throws an exception.
 *
 * The function will throw if \a j does not represent a JSON object. The
 * function can also throw any conversion exceptions.
 *
 * @tparam T The type which to convert the JSON field if found.
 * @param j[in] The JSON object from which to reference \a field.
 * @param field[in] The field in the parent JSON object to lookup.
 * @return T The converted field.
 * @throw expected_object, expected_field
 */
template<typename T>
T inline
get ( const nlohmann::json& j, const std::string& field ) {
	expect_field(j, field);
	return j.at(field).get<T>();
}

/**
 * @brief Attempt to find and convert \a field in JSON object \a j and store
 * the converted JSON in \a ret, otherwise leaves \a ret unchanged.
 *
 * The function does not throw if the field does not exist, though will throw
 * if \a j does not represent an object or if the value at \a field cannot be
 * converted to an object of type T.
 *
 * @tparam T The type to which the JSON field will be converted to if found.
 * @param j[in] The JSON object from which to reference \a field.
 * @param field[in] The field in the parent JSON object to lookup.
 * @param ret[out] The optional object used to store the converted JSON.
 * @throw expected_object
 */
template<typename T>
void inline
get ( const nlohmann::json& j,
      const std::string& field,
      std::optional<T>& ret ) {
	expect_object(j);
	if(j.contains(field)) {
		ret = j.at(field).get<T>();
	}
}

/**
 * @brief Converts the first field in \a fields that matches a corresponding
 * field in \a j and returns the converted value, otherwise throws an exception
 * if no matches were found.
 *
 * The function will throw if \a j does not represent a JSON object. The
 * function can also throw any conversion exceptions.
 *
 * @tparam T The type to which the JSON a field will be converted to if a match
 * is found.
 * @param j[in] The JSON object to scan and from which to covert matched fields.
 * @param fields[in] The list of fields to search.
 * @return T The converted field if found.
 * @throw expected_object, expected_at_least_one_of_fields
 */
template<typename T>
T inline
get ( const nlohmann::json& j,
      const std::vector<std::string>& fields ) {
	expect_object(j);

	for(auto field : fields) {
		if(has_field(j, field)) {
			return j.at(field).get<T>();
		}
	}

	throw expected_at_least_one_of_fields(j, fields);
}

/**
 * @brief Converts the first field in \a fields that matches a corresponding
 * field in \a j and stores the converted value in \a ret, otherwise leaves
 * \a ret unchanged.
 *
 * The function does not throw if none of the fields are found in \a j, though
 * will throw if \a j does not represent an object or if the value at the first
 * field found cannot be converted to an object of type T.
 *
 * @tparam T The type to which the JSON a field will be converted to if a match
 * is found.
 * @param j[in] The JSON object to scan and from which to covert matched fields.
 * @param fields[in] The list of fields to search.
 * @param ret[out] The optional object used to store the converted JSON.
 * @throw expected_object
 */
template<typename T>
void inline
get ( const nlohmann::json& j,
      const std::vector<std::string>& fields,
      std::optional<T>& ret ) {
	expect_object(j);

	for(auto field : fields) {
		if(j.contains(field)) {
			ret = j.at(field).get<T>();
			return;
		}
	}
}

/**
 * @brief Attempt to convert \a j to T, otherwise throws an exception.
 *
 * The function will throw if \a j does not represent a JSON object if there are
 * any conversion errors.
 *
 * @tparam T The type into which to convert the JSON object.
 * @param j[in] The JSON object to convert.
 * @return T The converted object.
 * @throw expected_object
 */
template<typename T>
T inline
get_object ( const nlohmann::json& j ) {
	expect_object(j);
	return j.get<T>();
}

/**
 * @brief Attempt to find and convert \a field in JSON object \a j and return
 * the converted T value, otherwise throws an exception.
 *
 * The function will throw if \a j does not represent a JSON object or if the
 * JSON value located at \a field does not represent a JSON object. The function
 * can also throw any conversion exceptions.
 *
 * @tparam T The type which to convert the JSON field if found.
 * @param j[in] The JSON object from which to reference \a field.
 * @param field[in] The field in the parent JSON object to lookup.
 * @return T The converted field.
 * @throw expected_object, expected_field, expected_object_field
 */
template<typename T>
T inline
get_object ( const nlohmann::json& j,
             const std::string& field ) {
	expect_object(j, field);
	return j.at(field).get<T>();
}

/**
 * @brief Attempt to convert \a j to T, otherwise throws an exception.
 *
 * The function will throw if \a j does not represent a JSON array if there are
 * any conversion errors.
 *
 * @tparam T The type into which to convert the JSON object.
 * @param j[in] The JSON array to convert.
 * @return T The converted object.
 * @throw expected_array
 */
template<typename T>
T inline
get_array ( const nlohmann::json& j ) {
	expect_array(j);
	return j.get<T>();
}

/**
 * @brief Attempt to find and convert \a field in JSON object \a j and return
 * the converted T value, otherwise throws an exception.
 *
 * The function will throw if \a j does not represent a JSON object or if the
 * JSON value located at \a field does not represent a JSON array. The function
 * can also throw any conversion exceptions.
 *
 * @tparam T The type which to convert the JSON field if found.
 * @param j[in] The JSON object from which to reference \a field.
 * @param field[in] The field in the parent JSON object to lookup.
 * @return T The converted field.
 * @throw expected_object, expected_field, expected_array_field
 */
template<typename T>
T inline
get_array ( const nlohmann::json& j,
            const std::string& field ) {
	expect_array(j, field);
	return j.at(field).get<T>();
}

/**
 * @brief Attempt to convert \a j to T, otherwise throws an exception.
 *
 * The function will throw if \a j does not represent a JSON string if there are
 * any conversion errors.
 *
 * @tparam T The type into which to convert the JSON object.
 * @param j[in] The JSON string to convert.
 * @return T The converted object.
 * @throw expected_string
 */
template<typename T>
T inline
get_string ( const nlohmann::json& j ) {
	expect_string(j);
	return j.get<T>();
}

/**
 * @brief Attempt to find and convert \a field in JSON object \a j and return
 * the converted T value, otherwise throws an exception.
 *
 * The function will throw if \a j does not represent a JSON object or if the
 * JSON value located at \a field does not represent a JSON string. The function
 * can also throw any conversion exceptions.
 *
 * @tparam T The type which to convert the JSON field if found.
 * @param j[in] The JSON object from which to reference \a field.
 * @param field[in] The field in the parent JSON object to lookup.
 * @return T The converted field.
 * @throw expected_object, expected_field, expected_string_field
 */
template<typename T>
T inline
get_string ( const nlohmann::json& j,
             const std::string& field ) {
	expect_string(j, field);
	return j.at(field).get<T>();
}

/**
 * @brief Attempt to convert \a j to T, otherwise throws an exception.
 *
 * The function will throw if \a j does not represent a JSON number if there are
 * any conversion errors.
 *
 * @tparam T The type into which to convert the JSON object.
 * @param j[in] The JSON number to convert.
 * @return T The converted object.
 * @throw expected_number
 */
template<typename T>
T inline
get_number ( const nlohmann::json& j ) {
	expect_number(j);
	return j.get<T>();
}

/**
 * @brief Attempt to find and convert \a field in JSON object \a j and return
 * the converted T value, otherwise throws an exception.
 *
 * The function will throw if \a j does not represent a JSON object or if the
 * JSON value located at \a field does not represent a JSON number. The function
 * can also throw any conversion exceptions.
 *
 * @tparam T The type which to convert the JSON field if found.
 * @param j[in] The JSON object from which to reference \a field.
 * @param field[in] The field in the parent JSON object to lookup.
 * @return T The converted field.
 * @throw expected_object, expected_field, expected_number_field
 */
template<typename T>
T inline
get_number ( const nlohmann::json& j,
             const std::string& field ) {
	expect_number(j, field);
	return j.at(field).get<T>();
}

template<typename T>
std::optional<T>&
get_enum(const nlohmann::json& j,
         const std::vector<std::pair<std::string,T>>& mapping) {
	const std::string str = j.get<std::string>();

	for(const auto& entry : mapping) {
		if(str.compare(entry.first) == 0) {
			return entry.second;
		}
	}
}

template<typename T>
void
get_enum(const nlohmann::json& j,
         const std::string& field,
         const std::vector<std::pair<std::string,T>>& mapping,
         std::optional<T>& e) {
	if(j.contains(field)) {
		auto val = j.at(field);
		if(val.is_string()) {
			e = get_enum(val, mapping);

			if(!e) {
				printf("Could not parse enu: %s\n",
				       field.c_str());
			}
		}
	}
	else{
		printf("Enum not found: %s\n", field.c_str());
		e = std::nullopt;
	}
}

template<typename T>
nlohmann::json
to_json ( const T& t ) {
	nlohmann::json j;
	to_json(j, t);
	return j;
}

std::optional<nlohmann::json>
read_json(const std::string& path);

template<typename T>
std::optional<nlohmann::json>
read_json(const std::optional<T>& path) {
	if(path) {
		return read_json(path.value());
	}

	return std::nullopt;
}
} // namespace tb
