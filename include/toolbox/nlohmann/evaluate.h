//
// Copyright (C) 2021 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <nlohmann/json.hpp>

namespace tb {
/**
 * @brief Checks if JSON \a j is an object.
 *
 * @param j[in] The JSON to evaluate.
 */
bool inline
is_object(const nlohmann::json& j);

/**
 * @brief Checks if JSON \a j is an array.
 *
 * @param j[in] The JSON to evaluate.
 */
bool inline
is_array(const nlohmann::json& j);

/**
 * @brief Checks if JSON \a j is a string.
 *
 * @param j[in] The JSON to evaluate.
 */
bool inline
is_string(const nlohmann::json& j);

/**
 * @brief Checks if JSON \a j is a number.
 *
 * @param j[in] The JSON to evaluate.
 */
bool inline
is_number(const nlohmann::json& j);

/**
 * @brief Checks if JSON \a j is an object and if it contains any fields
 * corresponding to \a field.
 *
 * @param j[in] The JSON to evaluate.
 * @param field[in] The field of \a j to evaluate.
 */
bool inline
has_field(const nlohmann::json& j, const std::string& field);

/**
 * @brief Checks if JSON \a j is an object which contains a field called
 * \a field that is an object.
 *
 * @param j[in] The JSON to evaluate.
 * @param field[in] The field of \a j to evaluate.
 */
bool inline
is_object(const nlohmann::json& j, const std::string& field);

/**
 * @brief Checks if JSON \a j is an object which contains a field called
 * \a field that is an array.
 *
 * @param j[in] The JSON to evaluate.
 * @param field[in] The field of \a j to evaluate.
 */
bool inline
is_array(const nlohmann::json& j, const std::string& field);

/**
 * @brief Checks if JSON \a j is an object which contains a field called
 * \a field that is a string.
 *
 * @param j[in] The JSON to evaluate.
 * @param field[in] The field of \a j to evaluate.
 */
bool inline
is_string(const nlohmann::json& j, const std::string& field);

/**
 * @brief Checks if JSON \a j is an object which contains a field called
 * \a field that is a number.
 *
 * @param j[in] The JSON to evaluate.
 * @param field[in] The field of \a j to evaluate.
 */
bool inline
is_number(const nlohmann::json& j, const std::string& field);
} // namespace tb
