//
// Copyright (C) 2021 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <nlohmann/json.hpp>

namespace tb {
/**
 * @brief Generic JSON exception.
 */
struct json_exception : public std::exception {
	std::string mesg;
	json_exception(const nlohmann::json& j) noexcept;

	json_exception(const nlohmann::json& j,
	               const std::string& prefix)  noexcept;

	virtual const char*
	what() const throw();
};

/**
 * @brief Exception thrown when a JSON is expected to be an object.
 */
struct expected_object : public json_exception {
	expected_object(const nlohmann::json& j) noexcept;
};

/**
 * @brief Exception thrown when a JSON is expected to be an array.
 */
struct expected_array : public json_exception {
	expected_array(const nlohmann::json& j) noexcept;
};

/**
 * @brief Exception thrown when a JSON is expected to be a string.
 */
struct expected_string : public json_exception {
	expected_string(const nlohmann::json& j) noexcept;
};

/**
 * @brief Exception thrown when a JSON is expected to be a number.
 */
struct expected_number : public json_exception {
	expected_number(const nlohmann::json& j) noexcept;
};

/**
 * @brief Exception thrown when a JSON is expected to be an object that contains
 * a specified field.
 */
struct expected_field : public json_exception {
	expected_field(const nlohmann::json& j,
	               const std::string& field) noexcept;

	expected_field(const nlohmann::json& j,
	               const std::string& field,
	               const std::string& prefix) noexcept;
};

/**
 * @brief Exception thrown when a JSON is expected to have at least one of the
 * fields in \a fields.
 */
struct expected_at_least_one_of_fields : public json_exception {
	std::vector<std::string> fields;
	expected_at_least_one_of_fields(const nlohmann::json& j,
	                                const std::vector<std::string>& fields)
	noexcept;
};

/**
 * @brief Exception thrown when a JSON is expected to be an object that contains
 * a specified field, where the field is an object.
 */
struct expected_object_field : public expected_field {
	expected_object_field(const nlohmann::json& j,
	                      const std::string& field) noexcept;
};

/**
 * @brief Exception thrown when a JSON is expected to be an object that contains
 * a specified field, where the field is an array.
 */
struct expected_array_field : public expected_field {
	expected_array_field(const nlohmann::json& j,
	                     const std::string& field) noexcept;
};

/**
 * @brief Exception thrown when a JSON is expected to be an object that contains
 * a specified field, where the field is a string.
 */
struct expected_string_field : public expected_field {
	expected_string_field(const nlohmann::json& j,
	                      const std::string& field) noexcept;
};

/**
 * @brief Exception thrown when a JSON is expected to be an object that contains
 * a specified field, where the field is a number.
 */
struct expected_number_field : public expected_field {
	expected_number_field(const nlohmann::json& j,
	                      const std::string& field) noexcept;
};

/**
 * @brief Throws an exception if JSON \a j is not an object.
 *
 * @param j[in] The JSON to evaluate.
 * @throw expected_object
 */
void
expect_object(const nlohmann::json& j);

/**
 * @brief Throws an exception if JSON \a j is not an array.
 *
 * @param j[in] The JSON to evaluate.
 * @throw expected_array
 */
void
expect_array(const nlohmann::json& j);

/**
 * @brief Throws an exception if JSON \a j is not a string.
 *
 * @param j[in] The JSON to evaluate.
 * @throw expected_string
 */
void
expect_string(const nlohmann::json& j);

/**
 * @brief Throws an exception if JSON \a j is not a number.
 *
 * @param j[in] The JSON to evaluate.
 * @throw expected_number
 */
void
expect_number(const nlohmann::json& j);

/**
 * @brief Throws an exception if JSON \a j is not an object or if JSON does not
 * contain any fields corresponding to \a field.
 *
 * @param j[in] The JSON to evaluate.
 * @param field[in] The field of \a j to evaluate.
 * @throw expected_field
 */
void
expect_field(const nlohmann::json& j, const std::string& field);

/**
 * @brief Throws an exception if JSON \a j is not an object, if JSON does not
 * contain any fields corresponding to \a field or if the JSON located at
 * \a field is not an object.
 *
 * @param j[in] The JSON to evaluate.
 * @param field[in] The field of \a j to evaluate.
 * @throw expected_object_field
 */
void
expect_object(const nlohmann::json& j, const std::string& field);

/**
 * @brief Throws an exception if JSON \a j is not an object, if JSON does not
 * contain any fields corresponding to \a field or if the JSON located at
 * \a field is not an array.
 *
 * @param j[in] The JSON to evaluate.
 * @param field[in] The field of \a j to evaluate.
 * @throw expected_array_field
 */
void
expect_array(const nlohmann::json& j, const std::string& field);

/**
 * @brief Throws an exception if JSON \a j is not an object, if JSON does not
 * contain any fields corresponding to \a field or if the JSON located at
 * \a field is not astring.
 *
 * @param j[in] The JSON to evaluate.
 * @param field[in] The field of \a j to evaluate.
 * @throw expected_string_field
 */
void
expect_string(const nlohmann::json& j, const std::string& field);

/**
 * @brief Throws an exception if JSON \a j is not an object, if JSON does not
 * contain any fields corresponding to \a field or if the JSON located at
 * \a field is not a number.
 *
 * @param j[in] The JSON to evaluate.
 * @param field[in] The field of \a j to evaluate.
 * @throw expected_number_field
 */
void
expect_number(const nlohmann::json& j, const std::string& field);
} // namespace tb
