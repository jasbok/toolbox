//
// Copyright (C) 2021 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <nlohmann/json.hpp>

namespace tb {
/**
 * @brief Converts a map to a JSON object.
 *
 * @tparam K Key type of map.
 * @tparam V Value type of map.
 * @param j[out] Destination object to store JSON.
 * @param t[in] Map to convert to JSON.
 */
template<typename K, typename V>
void
to_json ( nlohmann::json& j, const std::unordered_map<K, V>& t ) {
	j = t;
}

/**
 * @brief Converts a JSON object to a map.
 *
 * @tparam K Key type of map.
 * @tparam V Value type of map.
 * @param j[in] JSON object to convert to type T.
 * @param t[out] Destination map to store converted JSON.
 */
template<typename K, typename V>
void
from_json ( const nlohmann::json& j, std::unordered_map<K,V>& t ) {
	if(j.is_object()) {
		t = j.get<std::unordered_map<K,V>>();
	}
}

/**
 * @brief Stores an optional type in a JSON object if present, otherwise ignores
 * object.
 *
 * @tparam T Type of the optional object.
 * @param j[out] Destination object to store JSON.
 * @param t[in] The optional object of type T to convert to JSON.
 */
template<typename T>
void
to_json ( nlohmann::json& j, const std::optional<T>& t ) {
	if(t) {
		to_json(j, t.value());
	}
}

/**
 * @brief Converts JSON to type T and stores in an optional object.
 *
 * @tparam T Type of the optional object, which has to be default constructible.
 * @param j[in] The JSON to convert to type T.
 * @param t[out] The optional object to store the converted JSON.
 */
template<typename T>
void
from_json ( const nlohmann::json& j, std::optional<T>& t ) {
	static_assert(
		std::is_default_constructible<T>(),
		"Type T must be default constructible.");

	T value = t.value_or(T());
	from_json(j, value);
	t = value;
}
} // namespace tb
