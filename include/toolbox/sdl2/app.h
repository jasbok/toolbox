//
// Copyright (C) 2021 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include "toolbox/common/coords.h"

#include <SDL2/SDL.h>

#include <string>

namespace tb {
class app {
protected:
	SDL_Window* window_ = nullptr;
	SDL_Surface* surface_ = nullptr;
	std::string title_ = "SDL2";
public:
	app() = default;

	virtual
	~app();

	void
	init(const std::string& title, const tb::dims2<int>& dims);

	virtual void
	run() = 0;
};
} // namespace tb
