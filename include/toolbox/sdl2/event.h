/*
 * Copyright (C) 2022 Stefan Alberts
 * This file is part of Toolbox.
 *
 * Toolbox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toolbox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include "toolbox/common/event.h"

#include <SDL_events.h>

namespace tb {

using sdl_event_handler = tb::event_handler<Uint32, const SDL_Event&>;
using sdl_mousebutton_handler = tb::event_handler<Uint8, const SDL_MouseButtonEvent&>;
using sdl_key_handler = tb::event_handler<SDL_Keycode, const SDL_KeyboardEvent&>;
using sdl_window_handler = tb::event_handler<Uint8, const SDL_WindowEvent&>;

class sdl_events {
public:

	bool
	poll(SDL_Event& event){
		return true;
	}
};

}
