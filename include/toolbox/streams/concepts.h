//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <concepts>
#include <optional>
#include <ranges>

namespace tb::streams {


template<typename OP, typename STREAM, typename ... ARGS>
concept item_op =
	std::invocable<OP, const typename STREAM::output_t&, ARGS...>;

template<typename OP, typename STREAM, typename RET>
concept recurrent_op =
	std::invocable<OP, const RET&, const typename STREAM::output_t&>
	&& requires(OP op, RET ret, typename STREAM::output_t it){
	{op(ret, it)} -> std::same_as<RET>;
};

template<typename OP, typename STREAM>
concept counter_op = std::invocable<OP, int>;

template<typename T>
concept stream = requires{
	typename std::remove_reference<T>::type::output_t;
};

template<typename T>
concept iterable = requires(T t){
	t.begin(); t.end();
}
|| requires(T t){
	std::string(t);
};

} // namespace tb::streams
