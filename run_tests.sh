#!/bin/sh
SRC_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
export TEST_DATA_DIR="$SRC_DIR/test/data"

meson build && cd build
ninja && ./doctests $@
