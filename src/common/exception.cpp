//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "toolbox/common/exception.h"

using namespace tb;

generic_exception::
generic_exception(const std::string& mesg)
	: mesg_(mesg)
{}

generic_exception::
generic_exception(const std::string& mesg, const exception& cause)
	: mesg_(mesg + "\n  cause: " + cause.what())
{}

const char *
generic_exception::
what() const throw() {
	return mesg_.c_str();
}
