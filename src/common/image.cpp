//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "toolbox/common/image.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

namespace tb {
image
create_image(const struct pixels& pixels) {
	auto data = std::make_unique<uint8_t[]>(area(pixels.dims));
	auto pixs = pixels;
	pixs.data = data.get();

	return {std::move(data), pixs};
}

image
read_image(const std::string& path) {
	int width, height, channels;
	unsigned char* data = stbi_load(
		path.c_str(),
		&width,
		&height,
		&channels,
		0);

	if(data == nullptr) {
		throw read_image_exception( "Failed to read image: " + path);
	}

	enum pixels::format format;
	switch(channels)
	{
	case 1: format = pixels::format::greyscale; break;
	case 3: format = pixels::format::rgb; break;
	case 4: format = pixels::format::rgba; break;
	default: throw read_image_exception( "Cannot use image with "
		                             + std::to_string(channels)
		                             + " channels: " + path);
	}

	pixels pixs {
		{width, height},
		format,
		pixels::type::unsigned_byte,
		data
	};

	return {std::unique_ptr<uint8_t[]>(data), pixs};
} // read_image

read_image_exception::
read_image_exception(const std::string& mesg)
	: mesg_(mesg)
{}

const char *
read_image_exception::
what() const throw() {
	return mesg_.c_str();
}
} // namespace tb
