//
// Copyright (C) 2021 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "toolbox/common/str.h"

#include <sstream>

using namespace std;

namespace tb
{
string
replace_one(const string& str,
            const string& instance,
            const string& replacement) {
	if(instance.length() == 0){
		return str;
	}

	stringstream ss;

	size_t pos1 = 0;
	size_t pos2 = str.find(instance);

	if( (pos2 != string::npos) && (pos2 < str.length() ) ){
		ss << str.substr(pos1, pos2 - pos1) << replacement;
		pos1 = pos2 + instance.length();
	}

	ss << str.substr(pos1);

	return ss.str();
}

string
replace_all(const string& str,
            const string& instance,
            const string& replacement) {
	if(instance.length() == 0){
		return str;
	}

	stringstream ss;

	size_t pos1 = 0;
	size_t pos2 = str.find(instance);

	while(pos2 != string::npos && pos2 < str.length() ){
		ss << str.substr(pos1, pos2 - pos1) << replacement;

		pos1 = pos2 + instance.length();
		pos2 = str.find(instance, pos1);
	}

	ss << str.substr(pos1);

	return ss.str();
}

string
replace_all(const string& str,
            const regex&  regex,
            const string& replacement) {
	return regex_replace(str, regex, replacement);
}

bool
match(const std::string& str,
      const std::regex&  regex) {
	return regex_match(str, regex);
}

string
find_one(const string& str,
         const regex&  regex) {
	smatch match;

	regex_search(str, match, regex);

	return match.str();
}

vector<string>
find_all(const string& str,
         const regex&  regex) {
	vector<string> matches;

	smatch match;
	auto str_ = str;

	while(regex_search(str_, match, regex) && match.length() > 0){
		matches.push_back(match.str() );
		str_ = match.suffix().str();
	}

	return matches;
}

vector<vector<string>>
find_all_groups(const string& str,
                const regex&  regex) {
	vector<vector<string>> matches;

	smatch match;
	auto str_ = str;

	while(regex_search(str_, match, regex) && match.length() > 0){
		matches.push_back({ match.begin(), match.end() });
		str_ = match.suffix().str();
	}

	return matches;
}

std::pair<std::string_view, std::string_view>
read_kv(const std::string_view& str, char separator){
	auto it = std::find(begin(str), end(str), separator);
	return {{begin(str), it}, it != end(str) ? string_view(it+1, end(str)) : ""};
}

std::pair<std::string_view, std::string_view>
read_kv_trim(const std::string_view& str, char sep){
	auto kv = read_kv(str, sep);
	return {trim(kv.first), trim(kv.second)};
}

vector<string>
split(const string_view& str,
      const string_view& separator) {
	if(separator.size() == 0){
		return { std::string(str) };
	}

	vector<string> res;

	size_t pos1 = 0;
	size_t pos2 = str.find(separator);

	while(pos2 != string::npos && pos2 < str.length() ){
		res.push_back(std::string(str.substr(pos1, pos2 - pos1)));

		pos1 = pos2 + separator.length();
		pos2 = str.find(separator, pos1);
	}

	res.push_back(std::string(str.substr(pos1)));

	return res;
}

std::vector<std::string>
split(const std::string& str,
      const std::string& separator,
      const std::vector<std::pair<std::string, std::string>>& braces) {
	if(separator.size() == 0){
		return { str };
	}

	std::vector<std::string> res;
	std::vector<std::pair<std::string, std::string>> stack;

	size_t pos1 = 0;
	size_t pos2 = 0;

	while(pos2 < str.size() ){
		auto substr = str.substr(pos2);

		if(!stack.empty() ){
			const auto& top = stack.back();

			if(starts_with(substr, top.second) ){
				pos2 += top.second.length();
				stack.pop_back();
			}
			else if(starts_with(substr, top.first) ){
				pos2 += top.first.length();
				stack.push_back(top);
			}
			else {
				++pos2;
			}
		}
		else if(starts_with(substr, separator) ){
			res.push_back(str.substr(pos1, pos2 - pos1) );
			pos1 = pos2 = pos2 + separator.length();
		}
		else {
			for(const auto& pair : braces){
				if(starts_with(substr, pair.first) ){
					pos2 += pair.first.length();
					stack.push_back(pair);
					break;
				}
			}

			if(stack.empty() ){
				++pos2;
			}
		}
	}

	res.push_back(str.substr(pos1) );

	return res;
} // split

string
join(const vector<string>& strings,
     const string&         separator) {
	stringstream ss;

	if(strings.size() > 0){
		ss << strings[0];

		for(unsigned int i = 1; i < strings.size(); i++){
			ss << separator << strings[i];
		}
	}

	return ss.str();
}

bool
contains(const string_view& str,
         const string&      instance) {
	return str.find(instance) != string::npos;
}

string_view
trim(const string_view& str) {
	if(str.empty()){ return str; }

	string_view::iterator start, end;
	for(start = str.begin(); iswspace(*start); ++start);
	for(end = str.end(); iswspace(*(end-1)); --end);
	return string_view(start, end);
}

vector<string>
trim(const vector<string>& strings) {
	vector<string> res; res.reserve(strings.size());

	for(const auto& str : strings){
		res.push_back(std::string(trim(str)));
	}

	return res;
}

std::vector<std::string>
split_and_trim(const std::string_view& str,
               const std::string_view& separator) {
	auto vec = split(str, separator);
	for(auto& item : vec){ item = trim(item); }
	return vec;
}

bool
starts_with(const string_view& str, char ch) {
	return !str.empty() && *str.begin() == ch;
}

bool
starts_with(const string_view& str, const string_view& seq) {
	if(seq.empty() ){
		return true;
	}
	else if(str.size() < seq.size() ){
		return false;
	}

	return str.find(seq) == 0;
}

bool
ends_with(const string_view& str, char ch) {
	return !str.empty() &&  *(str.end() - 1)  == ch;
}

bool
ends_with(const string_view& str, const string_view& seq) {
	if(seq.empty() ){
		return true;
	}
	else if(str.size() < seq.size() ){
		return false;
	}

	for(unsigned int i = 1; i < seq.length() + 1; i++){
		if(*(str.end() - i) != *(seq.end() - i) ){
			return false;
		}
	}

	return true;
}

bool
equals(const std::string_view& a, const std::string_view& b) {
	return a.compare(b) == 0;
}
} // namespace tb
