//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "toolbox/libretro/slang_shader.h"

#include "toolbox/common/str.h"

#include <algorithm>
#include <charconv>
#include <exception>
#include <optional>
#include <stdexcept>

namespace tb {

template<typename T>
T
from_chars(const std::string_view& sv){
	T ret; auto res = std::from_chars(sv.data(), sv.data() + sv.size(), ret);

	if(res.ec == std::errc::invalid_argument){
		throw std::runtime_error("Invalid number: " + std::string(sv));
	}

	return ret;
}

bool
read_bool(const std::string_view& value){
	std::string lower;
	std::transform(begin(value), end(value), back_inserter(lower), tolower);
	return lower == "true";
}

enum slangp::shader::scale_type
read_scale_type(const std::string_view& value){
	std::string type;
	std::transform(begin(value), end(value), back_inserter(type), tolower);

	if(type == "source"){ return slangp::shader::scale_type::source; }
	if(type == "viewport"){ return slangp::shader::scale_type::viewport; }
	if(type == "absolute"){ return slangp::shader::scale_type::absolute; }

	throw std::runtime_error("Invalid scale_type: " + std::string(value));
}

std::optional<int>
get_index(const std::string_view& key, const std::string_view& name){
	if(!key.starts_with(name)){ return {}; }
	return {from_chars<int>(key.substr(name.size()))};
}

bool
set_shader_value(slangp& sp,
                 const std::string_view& key,
                 const std::string_view& val){

	if(auto idx = get_index(key, "shader")){
		sp.shaders[*idx].shader = val;
	}

	else if(auto idx = get_index(key, "alias")){
		sp.shaders[*idx].alias = val;
	}

	else if(auto idx = get_index(key, "filter_linear")){
		sp.shaders[*idx].filter_linear = read_bool(val);
	}

	else if(auto idx = get_index(key, "mipmap_input")){
		sp.shaders[*idx].mipmap_input = read_bool(val);
	}

	else if(auto idx = get_index(key, "float_framebuffer")){
		sp.shaders[*idx].float_framebuffer = read_bool(val);
	}

	else if(auto idx = get_index(key, "scale_type_x")){
		sp.shaders[*idx].scale_type_x = read_scale_type(val);
	}

	else if(auto idx = get_index(key, "scale_type_y")){
		sp.shaders[*idx].scale_type_y = read_scale_type(val);
	}

	else if(auto idx = get_index(key, "scale_type")){
		sp.shaders[*idx].scale_type = read_scale_type(val);
	}

	else if(auto idx = get_index(key, "scale_x")){
		sp.shaders[*idx].scale_x = from_chars<float>(val);
	}

	else if(auto idx = get_index(key, "scale_y")){
		sp.shaders[*idx].scale_y = from_chars<float>(val);
	}

	else if(auto idx = get_index(key, "scale")){
		sp.shaders[*idx].scale = from_chars<float>(val);
	}

	else{ return false; }

	return true;
} // set_shader_value

bool
set_texture_value(slangp& sp,
                  const std::string_view& key,
                  const std::string_view& val){
	for(auto& tex : sp.textures){
		if(key.starts_with(tex.alias)){
			auto var = key.substr(tex.alias.size());
			if(var.empty()){ tex.path = val; return true; }
			if(var == "_linear"){ tex.linear = read_bool(val); return true; }
		}
	}
	return false;
}

slangp
read_slangp(const std::string path){
	slangp sp;

	using sv = const std::string_view;
	read_ini(path, [&](sv& _, sv& key, sv& val){
		try {
			// Read number of shaders.
			if(key == "shaders"){ sp.shaders.resize(from_chars<int>(val)); return; }

			// Read shader parameters.
			if(set_shader_value(sp, key, val)){ return; };

			// Read number of textures.
			if(key == "textures"){
				for(const auto& name : tb::split_and_trim(val, ";")){
					sp.textures.push_back({.alias = name});
				}
				return;
			}

			// Read texture parameters.
			if(set_texture_value(sp, key, val)){ return; }

			printf("Unknown key found in slangp config: %s -> %s = %s",
			       path.c_str(), key.data(), val.data());
		}
		catch(std::runtime_error& ex){
			std::throw_with_nested(
				std::runtime_error("Could not read slangp config entry: " +
				                   std::string(key)));
		}
	});

	return sp;
}
} // namespace tb
