//
// Copyright (C) 2021 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "toolbox/nlohmann/except.h"

namespace tb {
using namespace nlohmann;

json_exception::json_exception(const json& j) noexcept
	: mesg("A JSON exception occurred.\nJSON:" + j.dump(2))
{}

json_exception::json_exception(const json& j,
                               const std::string& prefix) noexcept
	: mesg(prefix + "JSON:" + j.dump())
{}

const char*
json_exception::what() const throw() {
	return mesg.c_str();
}

expected_object::expected_object(const json&j) noexcept
	: json_exception(j, "Expected JSON object:\n")
{}


expected_array::expected_array(const json&j) noexcept
	: json_exception(j, "Expected JSON array:\n")
{}


expected_string::expected_string(const json&j) noexcept
	: json_exception(j, "Expected JSON string:\n")
{}

expected_number::expected_number(const json&j) noexcept
	: json_exception(j,"Expected JSON number:\n")
{}

expected_field::expected_field(const json& j,
                               const std::string& field) noexcept
	: json_exception(j,
	                 "Expected JSON object to have field.\nField: " + field +
	                 "\n")
{}

expected_field::expected_field(const json& j,
                               const std::string& field,
                               const std::string& prefix) noexcept
	: json_exception(j)
{
	mesg = prefix + "Field: " + field + "\n" + mesg;
}

std::string
expected_at_least_one_of_fields_prefix(const std::vector<std::string>& fields) {
	std::string fields_out = "";

	size_t i = 0;
	for(; i < fields.size(); i++) {
		fields_out += "'" + fields[i] + "', ";
	}
	fields_out += "'" + fields[i] + "'";

	return "Expected at least one field from: [" + fields_out + "]\n";
}

expected_at_least_one_of_fields::
expected_at_least_one_of_fields(const nlohmann::json& j,
                                const std::vector<std::string>& fields)
noexcept: json_exception(j, expected_at_least_one_of_fields_prefix(fields)) {}

expected_object_field::expected_object_field(const json& j,
                                             const std::string& field)
noexcept:
	expected_field(j, field,
	               "Expected JSON object to contain an object field:\n")
{}

expected_array_field::expected_array_field(const json& j,
                                           const std::string& field)
noexcept:
	expected_field(j, field,
	               "Expected JSON object to contain an array field:\n")
{}

expected_string_field::expected_string_field(const json& j,
                                             const std::string& field)
noexcept:
	expected_field(j, field,
	               "Expected JSON object to contain a string field:\n")
{}


expected_number_field::expected_number_field(const json& j,
                                             const std::string& field)
noexcept:
	expected_field(j, field,
	               "Expected JSON object to contain a number field:\n")
{}


void
expect_object(const json& j) {
	if(!j.is_object()) {
		throw expected_object(j);
	}
}

void
expect_array(const json& j) {
	if(!j.is_array()) {
		throw expected_array(j);
	}
}

void
expect_string(const json& j) {
	if(!j.is_string()) {
		throw expected_string(j);
	}
}

void
expect_number(const json& j) {
	if(!j.is_number()) {
		throw expected_number(j);
	}
}

void
expect_field(const json& j, const std::string& field) {
	expect_object(j);

	if(!j.contains(field)) {
		throw expected_field(j, field);
	}
}

void
expect_object(const json& j, const std::string& field) {
	expect_field(j, field);

	if(!j[field].is_object()) {
		throw expected_object_field(j, field);
	}
}

void
expect_array(const json& j, const std::string& field) {
	expect_field(j, field);

	if(!j[field].is_array()) {
		throw expected_array_field(j, field);
	}
}

void
expect_string(const json& j, const std::string& field) {
	expect_field(j, field);

	if(!j[field].is_string()) {
		throw expected_string_field(j, field);
	}
}

void
expect_number(const json& j, const std::string& field) {
	expect_field(j, field);

	if(!j[field].is_number()) {
		throw expected_number_field(j, field);
	}
}
} // namespace tb
