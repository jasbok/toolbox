//
// Copyright (C) 2021 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "toolbox/sdl2/app.h"

#include "toolbox/sdl2/exception.h"

namespace tb {
app::
~app() {
	if(window_) {
		SDL_DestroyWindow(window_);
	}

	if(surface_) {}

	SDL_Quit();
}

void
app::
init(const std::string& title, const tb::dims2<int>& dims) {
	title_ = title;

	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		throw sdl_exception("Failed to initialize SDL.");
	}

	window_ = SDL_CreateWindow( title_.c_str(),
	                            SDL_WINDOWPOS_UNDEFINED,
	                            SDL_WINDOWPOS_UNDEFINED,
	                            dims.x,
	                            dims.y,
	                            SDL_WINDOW_SHOWN );

	if(window_ == nullptr) {
		throw sdl_exception("Failed to create window.");
	}

	surface_ = SDL_GetWindowSurface( window_ );
}
} // namespace tb
