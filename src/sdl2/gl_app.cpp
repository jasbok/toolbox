//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "toolbox/sdl2/gl_app.h"

#include "toolbox/sdl2/exception.h"

#include <glbinding/glbinding.h>

namespace tb {

gl_app::~gl_app() {
  if(gl_context_) {
    SDL_GL_DeleteContext(gl_context_);
  }

  if(window_) {
    SDL_DestroyWindow(window_);
  }

  SDL_Quit();
}

void
gl_app::init(const std::string& title,
             const tb::dims2<int>& dims,
             int gl_major,
             int gl_minor) {
  title_ = title;

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor);

  if(SDL_Init(SDL_INIT_VIDEO) < 0) {
    throw sdl_exception("Failed to initialize SDL.");
  }

  window_ = SDL_CreateWindow(title_.c_str(),
                             SDL_WINDOWPOS_UNDEFINED,
                             SDL_WINDOWPOS_UNDEFINED,
                             dims.x,
                             dims.y,
                             SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

  if(window_ == nullptr) {
    throw sdl_exception("Failed to create window.");
  }

  gl_context_ = SDL_GL_CreateContext(window_);

  if(gl_context_ == nullptr)
  {
    throw sdl_exception("Failed to create OpenGL context.");
  }

  // Silents warning...
  auto get_proc_address = SDL_GL_GetProcAddress;
  glbinding::initialize(0, get_proc_address);

  post_init();
} // gl_app::init

} // namespace tb
