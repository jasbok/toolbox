//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "toolbox/spirv/shaderc.h"

#include "toolbox/common/file.h"

#include <exception>
#include <memory>
#include <shaderc/shaderc.hpp>

namespace tb {

class includer : public ::shaderc::CompileOptions::IncluderInterface {
	const std::string_view working_dir_;

	struct include_data {
		std::string source;
		std::string content;
		shaderc_include_result sir;
		include_data(const std::string& src, const std::string& cont)
			: source(src),
			  content(cont),
			  sir{source.c_str(), source.size(), content.c_str(), content.size(), nullptr}
		{}
	};

	using include = std::unique_ptr<include_data>;
	std::vector<include> includes_;

public:
	includer(const std::string_view& working_dir) : working_dir_(working_dir){}

	shaderc_include_result*
	GetInclude(const char *requested_source,
	           shaderc_include_type type,
	           const char *requesting_source,
	           size_t include_depth) override {
		try{
			std::string path = std::string(working_dir_) + requested_source;
			auto contents = tb::read_file(path);
			includes_.push_back(make_unique<include_data>(path, contents));
		}
		catch(std::exception& ex){
			includes_.push_back(std::make_unique<include_data>("", ex.what()));
		}
		return &includes_.back()->sir;
	}

	void
	ReleaseInclude(shaderc_include_result *data) override {
		auto rem = std::remove_if(begin(includes_), end(includes_), [&data](include& i){
			return &i->sir == data;
		});
		includes_.erase(rem, end(includes_));
	}
};

shaderc::shaderc(const std::string& working_dir) : working_dir_(working_dir){}

std::string
shaderc::preprocess_shader(shaderc_shader_kind kind,
                           const std::string& name,
                           const std::string& source) {
	::shaderc::Compiler compiler;
	::shaderc::CompileOptions options;
	options.SetIncluder(std::make_unique<includer>(working_dir_));

	::shaderc::PreprocessedSourceCompilationResult result =
		compiler.PreprocessGlsl(source, kind, name.c_str(), options);

	if(result.GetCompilationStatus() != shaderc_compilation_status_success){
		throw std::runtime_error("Preprocess for shader failed: "
		                         + result.GetErrorMessage());
	}

	return {result.cbegin(), result.cend()};
}

std::vector<uint32_t>
shaderc::compile_file(shaderc_shader_kind kind,
                      const std::string& name,
                      const std::string& source,
                      bool optimize) {
	::shaderc::Compiler compiler;
	::shaderc::CompileOptions options;
	options.SetIncluder(std::make_unique<includer>(working_dir_));

	if(optimize){ options.SetOptimizationLevel(shaderc_optimization_level_size); }

	::shaderc::SpvCompilationResult module =
		compiler.CompileGlslToSpv(source, kind, name.c_str(), options);

	if(module.GetCompilationStatus() != shaderc_compilation_status_success){
		throw std::runtime_error("Compilation for shader failed: " +
		                         module.GetErrorMessage());
	}

	return {module.cbegin(), module.cend()};
}

} // namespace tb
