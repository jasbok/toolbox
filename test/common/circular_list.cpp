//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "test/tests.h"

#include "toolbox/common/circular_list.h"

#include <string>

using namespace tb;

TEST_CASE("circular_list") {
	circular_list<std::string> list;
	REQUIRE(list.size() == 0);

	SUBCASE("Add four items sequentially.") {
		list.push_back("a");
		list.push_back("b");
		list.push_back("c");
		list.push_back("d");
		REQUIRE(list.size() == 4);
		CHECK(list.cursor() == 0);
		CHECK(list.get() == "a");
	}

	SUBCASE("Add four items using chaining.") {
		list.push_back("a")
		.push_back("b")
		.push_back("c")
		.push_back("d");

		REQUIRE(list.size() == 4);
		CHECK(list.cursor() == 0);
		CHECK(list.get() == "a");
	}

	SUBCASE("Cycle forward two times.") {
		list.push_back("a").push_back("b").push_back("c");
		REQUIRE(list.size() == 3);

		CHECK(list.cursor() == 0);
		CHECK(list.get() == "a");

		CHECK(list.next() == "b");
		CHECK(list.cursor() == 1);
		CHECK(list.get() == "b");

		CHECK(list.next() == "c");
		CHECK(list.cursor() == 2);
		CHECK(list.get() == "c");

		CHECK(list.next() == "a");
		CHECK(list.cursor() == 0);
		CHECK(list.get() == "a");

		CHECK(list.next() == "b");
		CHECK(list.cursor() == 1);
		CHECK(list.get() == "b");

		CHECK(list.next() == "c");
		CHECK(list.cursor() == 2);
		CHECK(list.get() == "c");

		CHECK(list.next() == "a");
		CHECK(list.cursor() == 0);
		CHECK(list.get() == "a");
	}

	SUBCASE("Cycle backwards two times.") {
		list.push_back("a").push_back("b").push_back("c");
		REQUIRE(list.size() == 3);

		CHECK(list.cursor() == 0);
		CHECK(list.get() == "a");

		CHECK(list.previous() == "c");
		CHECK(list.cursor() == 2);
		CHECK(list.get() == "c");

		CHECK(list.previous() == "b");
		CHECK(list.cursor() == 1);
		CHECK(list.get() == "b");

		CHECK(list.previous() == "a");
		CHECK(list.cursor() == 0);
		CHECK(list.get() == "a");

		CHECK(list.previous() == "c");
		CHECK(list.cursor() == 2);
		CHECK(list.get() == "c");

		CHECK(list.previous() == "b");
		CHECK(list.cursor() == 1);
		CHECK(list.get() == "b");

		CHECK(list.previous() == "a");
		CHECK(list.cursor() == 0);
		CHECK(list.get() == "a");
	}
}
