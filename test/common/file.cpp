//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "test/tests.h"

#include "toolbox/common/file.h"

using namespace tb;
using namespace std;

TEST_CASE (
	"tb::file: Read the contents of a file." )
{
	CHECK_THROWS(read_file("missing"));
	CHECK_THROWS(read_lines("missing"));

	auto test_file = test_file_path("test.txt");

	std::string contents =
		"This is a test file.\n"
		"The file has multiple lines.\n\n"
		"And ends with a new line.\n";
	CHECK(read_file(test_file) == contents);

	std::vector<std::string> lines = {
		"This is a test file.",
		"The file has multiple lines.",
		"",
		"And ends with a new line."
	};
	CHECK(read_lines(test_file) == lines);
}
