//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "test/tests.h"

#include "toolbox/common/image.h"

TEST_CASE("tb::image: Load image.") {
	auto path = test_file_path("keen4.gif");
	auto gif = tb::read_image(path);

	CHECK(gif.pixels.data != nullptr);
	CHECK(gif.pixels.dims.x == 320);
	CHECK(gif.pixels.dims.y == 200);
	CHECK(gif.pixels.format == tb::pixels::format::rgba);
	CHECK(gif.pixels.type == tb::pixels::type::unsigned_byte);
}
