//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "test/tests.h"

#include "toolbox/common/ini.h"

TEST_CASE("tb::ini: Read an ini file (sections)."){
	auto path = test_file_path("test.ini");
	auto ini = tb::read_ini(path);

	REQUIRE(ini.sections.size() == 3);

	CHECK(ini.sections[0].name.empty());
	auto kvs = ini.sections[0].key_values;
	REQUIRE(kvs.size() == 7);
	CHECK(kvs[0].key == "key0"); CHECK(kvs[0].value == "123");
	CHECK(kvs[1].key == "key1"); CHECK(kvs[1].value == "abc");
	CHECK(kvs[2].key == "key2"); CHECK(kvs[2].value == "false");
	CHECK(kvs[3].key == "key3"); CHECK(kvs[3].value == "default3");

	CHECK(ini.sections[1].name == "section_a");
	kvs = ini.sections[1].key_values;
	REQUIRE(kvs.size() == 6);
	CHECK(kvs[0].key == "key0a"); CHECK(kvs[0].value == "duplicate0");
	CHECK(kvs[1].key == "key1a"); CHECK(kvs[1].value == "abc");
	CHECK(kvs[2].key == "key2a"); CHECK(kvs[2].value == "true");
	CHECK(kvs[3].key == "key3a"); CHECK(kvs[3].value == "123");
	CHECK(kvs[4].key == "key4a"); CHECK(kvs[4].value == "duplicate4");
	CHECK(kvs[5].key == "key5a"); CHECK(kvs[5].value == "true");

	CHECK(ini.sections[2].name == "section b");
	kvs = ini.sections[2].key_values;
	REQUIRE(kvs.size() == 3);
	CHECK(kvs[0].key == "key0b"); CHECK(kvs[0].value == "123");
	CHECK(kvs[1].key == "key1b"); CHECK(kvs[1].value == "abc");
	CHECK(kvs[2].key == "key2b"); CHECK(kvs[2].value == "true");

	CHECK(ini[""]["key0"] == "123");
	CHECK(ini[""]["key3"] == "default3");
	CHECK(ini["section_a"]["key0a"] == "duplicate0");
	CHECK(ini["section b"]["key2b"] == "true");

	CHECK(ini[""]["key4"] == "quoted");
	CHECK(ini[""]["key5"] == "quoted1;quoted2;quoted3");
	CHECK(ini[""]["key6"] == "quoted1;quoted2;quoted3");

	CHECK_THROWS(ini["missing"]["key"]);
	CHECK_THROWS(ini[""]["missing"]);

	for(tb::ini::section& s : ini){
		CHECK(s.name == ini[s.name].name);
		for(tb::ini::kv& kv : s){
			CHECK(kv.value == s[kv.key]);
		}
	}
}

TEST_CASE("tb::ini: Read an ini file (slangp)."){
	auto path = test_file_path("test.slangp");
	auto ini = tb::read_ini(path);

	REQUIRE(!ini.sections.empty());

	CHECK(ini.sections[0].name.empty());
	auto kvs = ini.sections[0].key_values;
	REQUIRE(kvs.size() == 12);

	CHECK(kvs[0].key == "shaders"); CHECK(kvs[0].value == "2");
	CHECK(kvs[1].key == "shader0"); CHECK(kvs[1].value == "shaders/pass1.slang");
	CHECK(kvs[2].key == "shader1"); CHECK(kvs[2].value == "shaders/pass2.slang");

	CHECK(kvs[3].key == "filter_linear0"); CHECK(kvs[3].value == "true");
	CHECK(kvs[4].key == "scale_type0"); CHECK(kvs[4].value == "viewport");
	CHECK(kvs[5].key == "scale0"); CHECK(kvs[5].value == "1.0");

	CHECK(kvs[6].key == "filter_linear1"); CHECK(kvs[6].value == "false");
	CHECK(kvs[7].key == "scale_type1"); CHECK(kvs[7].value == "source");
	CHECK(kvs[8].key == "scale1"); CHECK(kvs[8].value == "2.0");

	CHECK(kvs[9].key == "textures"); CHECK(kvs[9].value == "LUT");
	CHECK(kvs[10].key == "LUT"); CHECK(kvs[10].value == "resources/lut.png");
	CHECK(kvs[11].key == "LUT_linear"); CHECK(kvs[11].value == "false");
}
