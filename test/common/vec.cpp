//
// Copyright (C) 2021 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "test/tests.h"

#include "toolbox/common/vec.h"

#include <string>

using namespace tb;
using namespace std;

TEST_CASE ( "toolbox: Append one vector to another." ) {
	vector<string> vec_empty = {};
	vector<string> vec_one   = { "a" };
	vector<string> vec_two   = { "a", "b" };
	vector<string> vec_three = { "a", "b", "c" };

	auto empty = vec_empty;

	append ( empty, vec_empty );
	CHECK ( empty == vector<string>{} );
	append ( empty, vec_one );
	CHECK ( empty == vector<string>{ "a" } );
	append ( empty, vec_two );
	CHECK ( empty == vector<string>{ "a", "a", "b" } );
	append ( empty, vec_three );
	CHECK ( empty == vector<string>{ "a", "a", "b", "a", "b", "c" } );
	REQUIRE ( vec_empty == vector<string>{} );
	REQUIRE ( vec_one   == vector<string>{ "a" } );
	REQUIRE ( vec_two   == vector<string>{ "a", "b" } );
	REQUIRE ( vec_three == vector<string>{ "a", "b", "c" } );

	auto one = vec_one;
	append ( one, vec_empty );
	CHECK ( one == vector<string>{ "a" } );
	append ( one, vec_one );
	CHECK ( one == vector<string>{ "a", "a" } );
	append ( one, vec_two );
	CHECK ( one == vector<string>{ "a", "a", "a", "b" } );
	append ( one, vec_three );
	CHECK ( one == vector<string>{ "a", "a", "a", "b", "a", "b", "c" } );
	REQUIRE ( vec_empty == vector<string>{} );
	REQUIRE ( vec_one   == vector<string>{ "a" } );
	REQUIRE ( vec_two   == vector<string>{ "a", "b" } );
	REQUIRE ( vec_three == vector<string>{ "a", "b", "c" } );

	auto two = vec_two;
	append ( two, vec_empty );
	CHECK ( two == vector<string>{ "a", "b" } );
	append ( two, vec_one );
	CHECK ( two == vector<string>{ "a", "b", "a" } );
	append ( two, vec_two );
	CHECK ( two == vector<string>{ "a", "b", "a", "a", "b" } );
	append ( two, vec_three );
	CHECK ( two ==
	        vector<string>{ "a", "b", "a", "a", "b", "a", "b", "c" } );
	REQUIRE ( vec_empty == vector<string>{} );
	REQUIRE ( vec_one   == vector<string>{ "a" } );
	REQUIRE ( vec_two   == vector<string>{ "a", "b" } );
	REQUIRE ( vec_three == vector<string>{ "a", "b", "c" } );

	auto three = vec_three;
	append ( three, vec_empty );
	CHECK ( three == vector<string>{ "a", "b", "c" } );
	append ( three, vec_one );
	CHECK ( three == vector<string>{ "a", "b", "c", "a" } );
	append ( three, vec_two );
	CHECK ( three == vector<string>{ "a", "b", "c", "a", "a", "b" } );
	append ( three, vec_three );
	CHECK ( three == vector<string>{ "a", "b", "c", "a", "a", "b", "a", "b",
	                                 "c" } );
	REQUIRE ( vec_empty == vector<string>{} );
	REQUIRE ( vec_one   == vector<string>{ "a" } );
	REQUIRE ( vec_two   == vector<string>{ "a", "b" } );
	REQUIRE ( vec_three == vector<string>{ "a", "b", "c" } );
}

TEST_CASE ( "toolbox: Cast vectors from one type to the next." ) {
	vector<int> empty_int = {};
	vector<int> one_int   = { 1 };
	vector<int> two_int   = { 1, 2 };
	vector<int> three_int = { 1, 2, 3 };

	vector<float> empty_float = {};
	vector<float> one_float   = { 1.1f };
	vector<float> two_float   = { 1.1f, 1.2f };
	vector<float> three_float = { 1.1f, 1.2f, 1.3f };

	CHECK ( cast<float>( empty_int ) ==  vector<float>{} );
	CHECK ( cast<float>( one_int ) == vector<float>{ 1.0f } );
	CHECK ( cast<float>( two_int ) == vector<float>{ 1.0f, 2.0f } );
	CHECK ( cast<float>( three_int ) == vector<float>{ 1.0f, 2.0f, 3.0f } );
	REQUIRE ( empty_int == vector<int>{} );
	REQUIRE ( one_int == vector<int>{ 1 } );
	REQUIRE ( two_int == vector<int>{ 1, 2 } );
	REQUIRE ( three_int == vector<int>{ 1, 2, 3 } );

	CHECK ( cast<int>( empty_float ) == vector<int>{} );
	CHECK ( cast<int>( one_float )   == vector<int>{ 1 } );
	CHECK ( cast<int>( two_float )   == vector<int>{ 1, 1 } );
	CHECK ( cast<int>( three_float ) == vector<int>{ 1, 1, 1 } );
	REQUIRE ( empty_float == vector<float>{} );
	REQUIRE ( one_float == vector<float>{ 1.1f } );
	REQUIRE ( two_float == vector<float>{ 1.1f, 1.2f } );
	REQUIRE ( three_float == vector<float>{ 1.1f, 1.2f, 1.3f } );
}
