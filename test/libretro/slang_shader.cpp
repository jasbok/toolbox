//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "test/tests.h"

#include "toolbox/libretro/slang_shader.h"

TEST_CASE("tb::slang_shader: Read Slangp file."){
	auto path = test_file_path("test2.slangp");

	tb::slangp slangp = tb::read_slangp(path);

	REQUIRE(slangp.shaders.size() == 10);

	using scale_type = enum tb::slangp::shader::scale_type;
	auto& shader = slangp.shaders[0];
	CHECK(shader.shader == "../stock.slang");
	CHECK(shader.filter_linear == false);
	CHECK(shader.scale_type == scale_type::source);
	CHECK(shader.scale == 1.0f);

	shader = slangp.shaders[1];
	CHECK(shader.shader == "../stock.slang");
	CHECK(shader.filter_linear == false);
	CHECK(shader.scale_type == scale_type::source);
	CHECK(shader.scale == 1.0f);
	CHECK(shader.alias == "Shader1Pass");

	shader = slangp.shaders[1];
	CHECK(shader.shader == "../stock.slang");
	CHECK(shader.filter_linear == false);
	CHECK(shader.scale_type == scale_type::source);
	CHECK(shader.scale == 1.0f);
	CHECK(shader.alias == "Shader1Pass");

	shader = slangp.shaders[2];
	CHECK(shader.shader == "shaders/shader2.slang");
	CHECK(shader.filter_linear == false);
	CHECK(shader.scale_type == scale_type::source);
	CHECK(shader.scale == 1.0f);
	CHECK(shader.alias == "Shader2Pass");

	shader = slangp.shaders[3];
	CHECK(shader.shader == "shaders/shader3.slang");
	CHECK(shader.filter_linear == false);
	CHECK(shader.scale_type == scale_type::source);
	CHECK(shader.scale == 1.0f);
	CHECK(shader.mipmap_input == true);
	CHECK(shader.alias == "Shader3Pass");

	shader = slangp.shaders[4];
	CHECK(shader.shader == "shaders/shader4.slang");
	CHECK(shader.filter_linear == true);
	CHECK(shader.scale_type == scale_type::source);
	CHECK(shader.scale == 1.0f);
	CHECK(shader.mipmap_input == true);
	CHECK(shader.alias == "Shader4Pass");

	shader = slangp.shaders[5];
	CHECK(shader.shader == "shaders/shader5.slang");
	CHECK(shader.filter_linear == true);
	CHECK(shader.scale_type == scale_type::source);
	CHECK(shader.scale == 1.0f);
	CHECK(shader.mipmap_input == false);
	CHECK(shader.alias == "Shader5Pass");
	CHECK(shader.float_framebuffer == true);

	shader = slangp.shaders[6];
	CHECK(shader.shader == "shaders/shader6.slang");
	CHECK(shader.filter_linear == true);
	CHECK(shader.scale_type_x == scale_type::absolute);
	CHECK(shader.scale_x == 800.0f);
	CHECK(shader.scale_type_y == scale_type::source);
	CHECK(shader.scale_y == 1.0f);
	CHECK(shader.mipmap_input == false);
	CHECK(shader.alias == "");
	CHECK(shader.float_framebuffer == true);

	shader = slangp.shaders[7];
	CHECK(shader.shader == "shaders/shader7.slang");
	CHECK(shader.filter_linear == true);
	CHECK(shader.scale_type_x == scale_type::absolute);
	CHECK(shader.scale_x == 800.0f);
	CHECK(shader.scale_type_y == scale_type::absolute);
	CHECK(shader.scale_y == 600.0f);
	CHECK(shader.mipmap_input == false);
	CHECK(shader.alias == "Shader7Pass");
	CHECK(shader.float_framebuffer == true);

	shader = slangp.shaders[8];
	CHECK(shader.shader == "shaders/shader8.slang");
	CHECK(shader.filter_linear == true);
	CHECK(shader.scale_type == scale_type::viewport);
	CHECK(shader.scale_x == 1.0f);
	CHECK(shader.scale_y == 1.0f);

	shader = slangp.shaders[9];
	CHECK(shader.shader == "shaders/shader9.slang");
	CHECK(shader.filter_linear == true);
	CHECK(shader.scale_type == scale_type::viewport);
	CHECK(shader.scale_x == 1.0f);
	CHECK(shader.scale_y == 1.0f);

	REQUIRE(slangp.textures.size() == 4);

	auto& texture = slangp.textures[0];
	CHECK(texture.alias == "SamplerLUT1");
	CHECK(texture.path == "shaders/SamplerLUT1.png");
	CHECK(texture.linear == true);

	texture = slangp.textures[1];
	CHECK(texture.alias == "SamplerLUT2");
	CHECK(texture.path == "shaders/SamplerLUT2.png");
	CHECK(texture.linear == true);

	texture = slangp.textures[2];
	CHECK(texture.alias == "SamplerLUT3");
	CHECK(texture.path == "shaders/SamplerLUT3.png");
	CHECK(texture.linear == true);

	texture = slangp.textures[3];
	CHECK(texture.alias == "SamplerLUT4");
	CHECK(texture.path == "shaders/SamplerLUT4.png");
	CHECK(texture.linear == true);
}
