#include "toolbox/sdl2/app.h"
#include <SDL2/SDL_timer.h>


class sdl_app : tb::app {
public:
	sdl_app()
	{
		init("SDL2 Test App", {640, 480});
	}

	void
	run() override {
		printf("Running SDL test app.\n");
		SDL_Delay(3000);
	}
};

int
main() {
	auto app = sdl_app();
	app.run();

	return 0;
}
