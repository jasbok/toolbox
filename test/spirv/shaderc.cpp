//
// Copyright (C) 2022 Stefan Alberts
// This file is part of Toolbox.
//
// Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
#include "test/tests.h"
#include "toolbox/spirv/shaderc.h"

#include "toolbox/common/file.h"

// TEST_CASE("toolbox:glslang: Build a spirv object from a glslang shader."){
// 	auto base = test_file_path("");
// 	auto path = test_file_path("test.slang");
// 	auto source = tb::read_file(path);

// 	tb::shaderc sc(base);
// 	auto preprocess = sc.preprocess_shader(
// 		shaderc_shader_kind::shaderc_glsl_vertex_shader,
// 		"test.slang",
// 		source);

// 	auto prog = sc.compile_file(shaderc_shader_kind::shaderc_glsl_vertex_shader,
// 	                            "test.slang",
// 	                            preprocess);
// }
