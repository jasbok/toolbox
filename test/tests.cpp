#include "tests.h"

const std::string data_dir = getenv("TEST_DATA_DIR");

std::string
test_file_path(const std::string& basepath) {
	return data_dir + "/" + basepath;
}
